// Exponent Operator
	// Create a variable getCube and use the exponent operator to compute for the cube of a number.

    let getCube = 2 ** 3;

// Template Literals
	// Using Template Literals, print out the value of the getCube variable with a message of the cube of <num> is..

    console.log(`The cube of 2 is ${getCube}`);

	// Create a variable address with a value of an array containing details of an address.
    // Destructuring the array and print out a message with the full address using Template Literals

    const address = [258, 'Washington Ave NW', 'California', 90011 ];

    const [homeNumber, aveName, stateName, zipCode] = address;

    console.log(`I live at ${homeNumber} ${aveName}, ${stateName} ${zipCode}`);

// Object Destructuring
	// Create a variable animal with the value of an object data type with different animals details as it's properties.
    // Destructuring the object and print out a message with the full address using Template Literals

    const animal = {
        name: 'Lolong',
        weight: 1075,
        lengthFt: 20,
        lengthInch: 3
    }

    const {name, weight, lengthFt, lengthInch} = animal;

    console.log(`${name} was a saltwater crocodile. He weighed at ${weight} kgs with a measurement of ${lengthFt} ft ${lengthInch} in.`);

// Arrow Function
	// create an array of numbers.

    const numbers = [1, 2, 3, 4, 5];

	// A. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

    numbers.forEach((number) => console.log(number));

	// B. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in array.

    const reduceNumber = numbers.reduce((x, y) => x + y);
    console.log(reduceNumber);


// JavaScript Classes
	// Create a class of a Dog and a constructor that will accept a name, age, and breed as its properties.
    // Create/instantiate a new object from the class Dog and console log the object.

    class Dog {
        constructor(name, age, breed) {
            this.name = name;
            this.age = age;
            this.breed = breed;
        }
    }

    const dog = new Dog('Frankie', 5, 'Miniature Dachshund');
    console.log(dog);