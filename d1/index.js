console.log('Hello World');

// ES6 Updates

// ES6 is one of the latest versions of writing JS and in fact is one of the major updates to JS.
// let, const - are ES6 updates, there are the new standards of creating variables


// Exponent Operator
	
	const firstNum = 8 ** 2;
	console.log(firstNum); // 64

	const secondNum = Math.pow(8,2);
	console.log(secondNum); // 64

	let string1 = 'fun';
	let string2 = 'Bootcamp';
	let string3 = 'Coding';
	let string4 = 'Javascript';
	let string5 = 'Zuitt';
	let string6 = 'Learning';
	let string7 = 'love';
	let string8 = 'I';
	let string9 = 'is';
	let string10 = 'in';
	/*
		Mini-activity 1
			1. create a new variable sentence1 and sentence2
			2. concatenate and save a resulting string into sentence1
			3. concatenate and save a resulting string into sentence2
				log both variables in the console.
				The sentences must have spaces and punctuation
	*/

		// let sentence1 = string5 + ' ' + string3 + ' ' + string2 + ' ' + string9 + ' ' +string1 + '!';
		// let sentence2 = string4 + ' ' + string9 + ' ' + string1 + '!';

		// console.log(sentence1); // Zuitt Coding Bootcamp is fun!
		// console.log(sentence2); // Javascript is fun!

// "", '' - string literals


// Template Literals
	// allows us to create strings using `` and easily embed JS expression in it
		/*
			- it allows us to write strings without using the concatenation(+)
			- greatly helps with the code readability
		*/

	let sentence1 = `${string5} ${string3} ${string2} ${string9} ${string1}!`;
	console.log(sentence1); // Zuitt Coding Bootcamp is fun!

	/*
		- ${} is a placeholder that is used to embed JS expressions when creating strings using Template Literals
	*/

	// Pre-Template Literals String
	// ("") or ('')

		let name = 'John';
		let message = 'Hello' + name + '! Welcome to programming!';

	// Strings using Template Literals
	// (``) backticks

		message = `Hello ${name}! Welcome to programming!`;
		console.log(message); // Hello John! Welcome to programming!

	// Multi-line using Template Literals

		const anotherMessage = `${name} attended a math competition. 
		He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
		console.log(anotherMessage);

		let dev = {
			name: 'Peter',
			lastName: 'Parker',
			occupation: 'web developer',
			income: 50000,
			expenses: 60000
		};

		console.log(`${dev.name} is a ${dev.occupation}`); // Peter is a web developer
		console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`); //His income is 50000 and expenses at 60000. His current balance is -10000.

		const interestRate = .1;
		const principal = 1000;

		console.log(`The interest on your savings account is ${principal * interestRate}`); //The interest on your savings account is 100

// Array Destructuring
	/*
		- Allows us to unpack elements in arrays into distinct variables

		Syntax
			let/const [variableNameA, variableNameB, variableNameC] = array;
	*/

	const fullName = ['Juan', 'Dela', 'Cruz'];

	// Pre-Array Destructuring
		console.log(fullName[0]); // Juan
		console.log(fullName[1]); // Dela
		console.log(fullName[2]); // Cruz

		console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`); // Hello Juan Dela Cruz! It is nice to meet you!

	// Array Destructuring 

		const [firstName, middleName, lastName] = fullName;

		console.log(firstName); // Juan
		console.log(middleName); // Dela
		console.log(lastName); // Cruz

		console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`); // Hello Juan Dela Cruz! It is nice to meet you!

// Object Destructuring
	/*
		- Allows us to unpack properties of objects into distinct variables

		Syntax
			let/const {propertyNameA, propertyNameB, propertyNameC} = object;
	*/

	const person = {
		givenName: 'Jane',
		maidenName: 'Dela',
		familyName: 'Cruz'
	};

	// Pre-Object Destructuring
		// we can access them using . or [] notation

		console.log(person.givenName); // Jane
		console.log(person.maidenName); // Dela
		console.log(person.familyName); // Cruz

	// Object Destructuring

		const {givenName, maidenName, familyName} = person;

		console.log(givenName); // Jane
		console.log(maidenName); // Dela
		console.log(familyName); // Cruz

// Arrow Functions

	/*
		- Compact alternative syntax to traditional functions

	*/

	const hello = () => {
		console.log('Hello World');
	}

	hello(); // Hello World

	// const hello = function hello1(){
	// 	console.log('Hello World');
	// }

	// hello(); // Hello World

	// Pre-Arrow Function and Template Literals
		/*
			Syntax
				function functionName(parameterA, parameterB, parameterC){
					console.log();
				}
		*/

	// Arrow Function
		/*
			Syntax
				let/const variableName = (parameterA, parameterB, parameterC) => {
					console.log();
				}
		*/

		const printFullName = (firstName, middleInitial, lastName) => {
			console.log(`${firstName} ${middleInitial}. ${lastName}`)
		};

		printFullName('John', 'D', 'Smith'); // John D. Smith

// Arrow Function with loops

	// Pre-Arrow Function

		const students = ['John', 'Jane', 'Judy'];

		students.forEach(function(student){
			console.log(`${student} is a student`);
			// John is a student
			// Jane is a student
			// Judy is a student
		});

	// Arrow Function

		students.forEach((student) => {
			console.log(`${student} is a student.`);
			// John is a student
			// Jane is a student
			// Judy is a student
		});

// Implicit Return Statement
	/*
		There are instances when you can omit 'return' statement
		This works because even without the 'return' statement JS IMPLICITLY adds it for the result of the function
	*/

	// Pre-arrow function

		// const add = (x, y) => {
		// 	return x + y;
		// }

			// {} in an arrow function are code block. if an arrow function has a {} or code block, we're going to use a return

			// implicit return will only work on arrow functions without {}

		const add = (x,y) => x + y;

		let total = add(1,2);
		console.log(total); // 3

		/*
			Mini Activity 2
				create a subtract, multiply, and divide arrow function
				use 1 and 2 as an arguments
		*/

		const subtract = (x,y) => x - y;

		let result1 = subtract(1,2);
		console.log(result1); // -1

		const multiply = (x,y) => x * y;

		let result2 = multiply(1,2);
		console.log(result2); // 2

		const divide = (x,y) => x / y;

		let result3 = divide(1,2);
		console.log(result3); // 0.5


// Default Function Argument Value
	// provide a default argument value if none is provided when the function is invoked

	const greet = (name = 'User') => {
		return `Good Evening, ${name}`;
	};

	console.log(greet()); // Good Evening, User
	console.log(greet('John')); // Good Evening, John


// Class-Based Object Blueprint
	/*
		- Allows creation/instantiation of objects using classes as blueprints
	*/

	// Creating a class
		/*
			- the constructor is a special method of a class for creating/initializing an object for that class

			Syntax
				class className {
					constructor(objectPropertyA, objectPropertyB) {
						this.objectPropertyA = objectobjectPropertyA
						this.objectPropertyB = objectobjectPropertyB
					}
				}
		*/

		class Car {
			constructor(brand,name,year){
				this.brand = brand;
				this.name = name;
				this.year = year;
			}
		}

	// Instantiating an object
		/*
			- the 'new' operator creates/instantiates a new object with the given arguments as the value of its properties

			Syntax
				let/const variableName = new className();
		*/
		/*
			- creating a constant with the 'const' keyword and assigning it a value of an object makes it so we can't reassign it with another data type
		*/

		const myCar = new Car();

		console.log(myCar); // Car {brand: undefined, name: undefined, year: undefined}

		myCar.brand = 'Ford';
		myCar.name = 'Everest';
		myCar.year = '1996';

		console.log(myCar); // Car {brand: 'Ford', name: 'Everest', year: '1996'}

		const myNewCar = new Car('Toyota', 'Vios', 2021);
		console.log(myNewCar); // Car {brand: 'Toyota', name: 'Vios', year: 2021}

	/*
		Mini activity 3
			create a character class constructor
			name:
			role:
			strength:
			weakness:

			create 2 new characters out of the class constructor and save it in their respective variables
	*/

		class Character {
			constructor(name, role, strenght, weakness) {
				this.name = name;
				this.role = role;
				this.strenght = strenght;
				this.weakness = weakness;
			}
		}

		const character1 = new Character('Starlight Anya', 'Mage', 'cuteness', 'Mama and Papa');
		// const character2 = new Character('')
		console.log(character1); // Character {name: 'Starlight Anya', role: 'Mage', strenght: 'cuteness', weakness: 'Mama and Papa'}
		// console.log(character2);


